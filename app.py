import os
#import configparser
from flask import Flask, redirect, request, url_for
import logging

# Log to app.log file
#logging.basicConfig(filename='app.log', filemode='w', format='%(name)s - %(levelname)s - %(message)s', level=logging.DEBUG)

# Log to STDOUT
logging.basicConfig(handlers=[logging.StreamHandler()], format='%(name)s - %(levelname)s - %(message)s', level=logging.DEBUG)

# Read config from vars.ini file
#config = configparser.ConfigParser()
#has_config = config.read('vars.ini')

# Set ENV variables instead of using vars.ini config file
#os.environ['ENVIRONMENT'] = 'DEV'
#os.environ['DISPLAY_FONT'] = 'arial'
#os.environ['DISPLAY_COLOR'] = 'red'

signatures = []

app = Flask(__name__)

# configurations from vars.ini
#font = config['style']['DISPLAY_FONT']
#font_color = config['style']['DISPLAY_COLOR']
#environment = config['debug']['ENVIRONMENT']

# configurations from ENV variables
font = os.getenv('DISPLAY_FONT')
font_color = os.getenv('DISPLAY_COLOR')
environment = os.getenv('ENVIRONMENT')

@app.route('/', methods=['GET'])
def index():
    html = """
    Signatures: <br />
    <font face="%(font)s" color="%(color)s">
        %(messages)s
    </font>
    <br /> <br />
    <form action="/signatures" method="post">
        Sign the Guestbook: <input type="text" name="message"><br>
        <input type="submit" value="Sign">
    </form>

    <br />
    <br />
    Debug Info: <br />
    ENVIRONMENT is %(environment)s
    """
    messages_html = "<br />".join(signatures)
    return html % {"font": font, "color": font_color, "messages": messages_html, "environment": environment}

@app.route('/signatures', methods=['POST'])
def write():
    message = request.form.get('message')
    signatures.append(message)

    return redirect(url_for('index'))

if __name__ == "__main__":
    app.run(host='0.0.0.0', port=8080)

