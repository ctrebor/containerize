FROM fedora:32
# TODO Set metadata for who maintains this image

RUN mkdir /app
COPY * /app/
RUN dnf install python3 python3-pip -y
RUN pip3 install Flask
EXPOSE 8080

#TODO Set default values for env variables
ENV DISPLAY_FONT=arial 
ENV DISPLAY_COLOR=blue
ENV ENVIRONMENT=DEV

#TODO *bonus* add a health check that tells docker the app is running properly

#TODO have the app run as a non-root user
RUN adduser applid
USER applid

CMD python3 /app/app.py
